from collections import OrderedDict
import math


def getData(inFile):
    lista = list()
    errors = 0
    loadedFloats = 0

    for line in inFile:
        for word in line.split():
            try:
                lista.append(float(word))
                loadedFloats += 1
            except ValueError:
                print('{} it s not a float'.format(word))
                errors += 1

    print('Loaded floats: {}'.format(loadedFloats))
    print('Incorrect values: {}'.format(errors))
    lista.sort()
    return lista


def sMin(dataList):
    return dataList[0]


def sMax(dataList):
    return dataList[-1]


def sRange(dataList):
    return dataList[-1] - dataList[0]


def sMean(dataList):
    sum = 0
    count = 0
    for value in dataList:
        sum += value
        count += 1
    return sum / count


def sPercentile(dataList, k):
    if k <= 0 or k > 100:
        raise ValueError
    i = int(math.ceil((k * len(dataList)) / 100))
    return dataList[i - 1]


def sMedian(dataList):
    return sPercentile(dataList, 50)


def sQuartiles(dataList):
    return (sPercentile(dataList, 25),
            sPercentile(dataList, 50),
            sPercentile(dataList, 75))


def sQuantiles(dataList, cutPoints):
    krotka = list()
    for cut in cutPoints:
        krotka.append(sPercentile(dataList, cut))
    return tuple(krotka)


def sMode(dataList):
    countTemp = 0
    count = 0
    mode = 0
    for i in range(0, len(dataList) - 1):
        if dataList[i] == dataList[i + 1]:
            k = i
            while k != len(dataList) and dataList[i] == dataList[k]:
                countTemp += 1
                k += 1
            i = k
            if countTemp > count:
                mode = dataList[i - 1]
                count = countTemp
                countTemp = 0
            else:
                countTemp = 0
    return mode


def sModes(dataList):
    countTemp = 0
    count = 0
    listReturn = list()
    for i in range(0, len(dataList) - 1):
        if dataList[i] == dataList[i + 1]:
            k = i
            while k != len(dataList) and dataList[i] == dataList[k]:
                countTemp += 1
                k += 1
            i = k
            if countTemp > count:
                listReturn = list()
                listReturn.append(dataList[i - 1])
                count = countTemp
                countTemp = 0
            elif countTemp == count:
                listReturn.append(dataList[i - 1])
                countTemp = 0
            else:
                countTemp = 0
    return listReturn


def variance(dataList):
    sum = 0
    average = sMean(dataList)
    for value in dataList:
        sum += float(value - average) ** 2
    return sum / (len(dataList) - 1)


def stdDeviation(dataList):
    return math.sqrt(variance(dataList))


def SimpleFrequencyDistribution(dataList):
    sfd = OrderedDict()
    for i in dataList:
        if i not in sfd.keys():
            sfd[i] = 1
        else:
            sfd[i] += 1
    return sfd


def printSimpleFrequencyDistribution(sfd, size):
    print('{0:-<{s}}---{0:-<{s}}'.format("", s=size))
    for key, value in sfd.items():
        print('{0: >{s}} | {1: >{s}}'.format(key, value, s=size))
    print('{0:-<{s}}---{0:-<{s}}'.format("", s=size))


if __name__ == "__main__":
    pliczek = open("floats.txt", "r")
    listka = getData(pliczek)
    list2 = [3, 6, 7, 8, 8, 9, 10, 10, 13, 13, 13, 15, 16, 16, 16, 20]
    print(listka)
    print('min is: {}'.format(sMin(listka)))
    print('max is: {}'.format(sMax(listka)))
    print('range is: {}'.format(sRange(listka)))
    print('average is: {}'.format(sMean(listka)))
    print('percentile is: {}'.format(sPercentile(list2, 50)))
    print('median is: {}'.format(sMedian(list2)))
    print('quartiles are: {}'.format(sQuartiles(list2)))
    print('quantiles are: {}'.format(sQuantiles(list2, (10, 25, 50, 75, 100))))
    print('mode is: {}'.format(sMode(list2)))
    print('modes is: {}'.format(sModes(list2)))
    print('variance is: {}'.format(variance(list2)))
    print('deviation is: {}'.format(stdDeviation(list2)))
    print('SimpleFrequencyDistribution is: {}'.format(SimpleFrequencyDistribution(list2)))
    printSimpleFrequencyDistribution(SimpleFrequencyDistribution(list2), 5)

