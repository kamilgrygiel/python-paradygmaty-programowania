import math
import sys


def plik(plik):
    lista = list()
    krotki = list()
    for line in plik:
        for word in line.split():
            krotki.append(float(word))
        lista.append(tuple(krotki))
        krotki = list()
    return lista


def disjoin(first, second):
    x1 = lista[first][0]
    x2 = lista[second][0]
    y1 = lista[first][1]
    y2 = lista[second][1]
    r1 = lista[first][2]
    r2 = lista[second][2]
    odleglosc = math.sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))

    if odleglosc > r1 + r2 or odleglosc < abs(r1 - r2):
        print("x1: {}, y1: {}, x2: {}, y2: {}".format(x1, y1, x2, y2))
        print("Okregi sa rozlaczne")
    else:
        print("x1: {}, y1: {}, x2: {}, y2: {}".format(x1, y1, x2, y2))
        print("Okregi nie sa rozlaczne")


lista = plik(open(sys.argv[1], "r"))
disjoin(0, 1)
disjoin(0, 2)
disjoin(3, 4)
disjoin(0, 0)