class Knight:
    def __init__(self):
        self._x = self._y = 1
        self._history = list()
        self._history.append(tuple((self._x, self._y)))

    @property
    def __str__(self):
        print(self._history)

    def move(self, x, y):
        possibleMoves = self.possibleMoves
        if tuple((x, y)) in possibleMoves:
            self._x = x
            self._y = y
            self._history.append(tuple((x, y)))
            print("git!")
        else:
            raise ValueError

    @property
    def possibleMoves(self):
        possibleMoves = list()
        if self._x - 1 >= 1 and self._y - 2 >= 1: possibleMoves.append(tuple((self._x - 1, self._y - 2)))
        if self._x - 1 >= 1 and self._y + 2 <= 8: possibleMoves.append(tuple((self._x - 1, self._y + 2)))
        if self._x - 2 >= 1 and self._y - 1 >= 1: possibleMoves.append(tuple((self._x - 2, self._y - 1)))
        if self._x - 2 >= 1 and self._y + 1 <= 8: possibleMoves.append(tuple((self._x - 2, self._y + 1)))
        if self._x + 1 <= 8 and self._y - 2 >= 1: possibleMoves.append(tuple((self._x + 1, self._y - 2)))
        if self._x + 1 <= 8 and self._y + 2 <= 8: possibleMoves.append(tuple((self._x + 1, self._y + 2)))
        if self._x + 2 <= 8 and self._y - 1 >= 1: possibleMoves.append(tuple((self._x + 2, self._y - 1)))
        if self._x + 2 <= 8 and self._y + 1 <= 8: possibleMoves.append(tuple((self._x + 2, self._y + 1)))
        return possibleMoves

test = Knight()
print(test.possibleMoves)
test.move(2, 3)
print(test.possibleMoves)
test.move(3, 5)
test.__str__