from collections import OrderedDict
from descstat import getData
import math


class DataList:
    def __init__(self, fileName):
        self.data = getData(fileName)

    @property
    def mean(self):
        sum = 0
        count = 0
        for value in self.data:
            sum += value
            count += 1
        return sum / count

    @property
    def min(self):
        return self.data[0]

    @property
    def max(self):
        return self.data[-1]

    @property
    def range(self):
        return self.data[-1] - self.data[0]

    @property
    def median(self):
        return self.percentile(50)

    @property
    def variance(self):
        sum = 0
        average = self.mean
        for value in self.data:
            sum += float(value - average) ** 2
        return sum / (len(self.data) - 1)

    @property
    def std(self):
        return math.sqrt(self.variance)

    @property
    def quartiles(self):
        return (self.percentile(25),
                self.percentile(50),
                self.percentile(75))

    def percentile(self, k):
        if k <= 0 or k > 100:
            raise ValueError
        i = int(math.ceil((k * len(self.data)) / 100))
        return self.data[i-1]

    def quantiles(self, cutPoints):
        krotka = list()
        for cut in cutPoints:
            krotka.append(self.percentile(cut))
        return tuple(krotka)

    @property
    def modes(self):
        countTemp = 0
        count = 0
        listReturn = list()
        for i in range(0, len(self.data) - 1):
            if self.data[i] == self.data[i + 1]:
                k = i
                while k != len(self.data) and self.data[i] == self.data[k]:
                    countTemp += 1
                    k += 1
                i = k
                if countTemp > count:
                    listReturn = list()
                    listReturn.append(self.data[i - 1])
                    count = countTemp
                    countTemp = 0
                elif countTemp == count:
                    listReturn.append(self.data[i - 1])
                    countTemp = 0
                else:
                    countTemp = 0
        return listReturn


class GroupedFrequencyDistribution:

    def __init__(self, dataList, start, end, numberOfGroups):
        size = (end - start) / numberOfGroups
        halfSize = size / 2
        groupStart = start
        groupMiddle = start + halfSize
        groupEnd = start + size
        groupCount = 0
        self.fdTable = OrderedDict()

        for i in dataList.data:
            if groupStart < i <= groupEnd:
                groupCount += 1
            else:
                self.fdTable[(groupStart, groupMiddle, groupEnd)] = groupCount
                groupStart = groupEnd
                groupMiddle = groupStart + halfSize
                groupEnd = groupStart + size
                while not (groupStart < i <= groupEnd):
                    self.fdTable[(groupStart, groupMiddle, groupEnd)] = 0
                    groupStart = groupEnd
                    groupMiddle = groupStart + halfSize
                    groupEnd = groupStart + size
                groupCount = 1
        self.fdTable[(groupStart, groupMiddle, groupEnd)] = groupCount
        self._precision = 0

    @property
    def precision(self):
        return self._precision

    @precision.setter
    def precision(self, prec):
        if prec < 0:
            self._precision = 0
        else:
            self._precision = int(prec)

    def __str__(self):
        _, _, r = next(reversed(self.fdTable))
        size = len(str(int(r))) + self.precision + 1

        fdt2str = '---{0:-<{s}}--{0:-<{s}}----{0:-<{s}}---{0:-<{s}}--\n'.format("", s=size)
        for (l, c, r), v in self.fdTable.items():
            fdt2str += '| ({0:{s1}.{s2}f}, {1:{s1}.{s2}f}) | {2:{s1}.{s2}f} | {3:{s1}} |\n'.format(l, r, c, v, s1=size,
                                                                                                  s2=self.precision)
        fdt2str += '---{0:-<{s}}--{0:-<{s}}----{0:-<{s}}---{0:-<{s}}--'.format("", s=size)
        return fdt2str

    @property
    def mean(self):
        sum = 0
        count = 0
        for (l, c, r), v in self.fdTable.items():
            sum += v
            count += c * v
        if count > 0:
            return sum / count
        else:
            raise ValueError

    @property
    def variance(self):
        sum = 0
        average = self.mean
        count = 0
        for (l, c, r), v in self.fdTable.items():
            sum += float((c - average) ** 2) * v
            count += v
        if count > 0:
            return sum / count
        else:
            raise ValueError

    @property
    def std(self):
        return math.sqrt(self.variance)


if __name__ == "__main__":
    dl = DataList(open("floats.txt", "r"))
    gfd = GroupedFrequencyDistribution(dl, 1, 13, 4)
    gfd.precision = 2
    print(gfd)
    print(dl.mean)
    print(gfd.mean)
    print(dl.variance)
    print(gfd.variance)
    print(dl.std)
    print(gfd.std)
