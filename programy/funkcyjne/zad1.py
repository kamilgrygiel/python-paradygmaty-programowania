l1 = [x for x in range(100,1000) if x % 2 == 1 and x % 17 == 0]
print(l1)
l2 = [x**2 for x in range(19) if 2**x < 50000]
print(l2)
l3 = [(x, x**2) for x in range(15) if 1000 < 2**x < 5000]
print(l3)
l4 = [(x,y) for x in 'ABCDEFGH' for y in range(1,9)]
print(l4)
l5 = [(x,y) for x in range(1,10) for y in range(1,10) if x*y < 10]
print(l5)
l6 = [(x,y,z) for x in range(1,10) for y in range(1,10) for z in range(1,10) if x != y and x*y < z]
print(l6)
l7 = [[x*y for x in range(1,11)]for y in range(1,11)]
print(l7)
