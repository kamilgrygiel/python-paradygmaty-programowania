from functools import reduce

l1 = sum(map(lambda x: 1 / x, range(1, 101)))
print(l1)
l2 = sum(map(lambda x: 1 / x ** 2, range(1, 1001)))
print(l2)
l3 = sum(map(lambda x: x ** (1 / 2) - 1 / x, range(1, 1001)))
print(l3)
l4 = reduce(lambda x, y: x * y, map(lambda x: (1 + x) / (2 + x), range(1, 51)))
print(l4)
l5 = reduce(lambda x, y: x * y, map(lambda x: (x + 1) / (x ** (1 / 2)), range(1, 11)))
print(l5)
l6 = (lambda x, y: x ** (1 / y))(*reduce(lambda x, y: (x[0] * y, x[1] + 1), [1, 2, 4, 6], (1, 0)))
print(l6)
