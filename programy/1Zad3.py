def prime(n, i):
    if i == 1:
        return 1
    else:
        if n % i == 0:
            return 0
        else:
            return prime(n, i-1)


n = int(input("Podaj liczbe naturalna"))
primeNum = prime(n, n/2)
if primeNum == 1:
    print('{} is prime'.format(n))
else:
    print('{} is not prime'.format(n))
