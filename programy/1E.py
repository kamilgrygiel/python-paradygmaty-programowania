def getData(inFile):
    listatmp = list()
    lista = list()
    errors = 0
    loadedInts = 0

    for line in inFile:

        for word in line.split():
            try:
                listatmp.append(int(word))
                loadedInts += 1
            except ValueError:
                print('{} it s not a int'.format(word))
                errors += 1
        lista.append(listatmp)
        listatmp = list()

    print('Loaded ints: {}'.format(loadedInts))
    print('Incorrect values: {}'.format(errors))
    return lista


def disjoin(listaa, listab):
    for a in listaa:
        for b in listab:
            if a == b:
                return False
    return True


if __name__ == "__main__":
    pliczek = open("ints.txt", "r")
    listka = getData(pliczek)
    print(listka)
    a = disjoin(listka[0], listka[2])
    print(a)
